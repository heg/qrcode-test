let video = document.createElement("video");
let canvasElement = document.getElementById("qr-canvas");
let canvas = canvasElement.getContext("2d");

let qrResult = document.getElementById("qr-result");
let qrResultTitle = document.getElementById("qr-result-title");
let outputData = document.getElementById("outputData");
let btnScanQR = document.getElementById("btn-scan-qr");
let btnScanQRStop = document.getElementById("btn-scan-qr-stop");

let scanning = false;

stopScan = () => {
  scanning = false;

  video.srcObject.getTracks().forEach(track => {
    track.stop();
  });

  btnScanQR.hidden = false;
  btnScanQRStop.hidden = true;
  canvasElement.hidden = true;
  // canvasElement.exitFullscreen();
}


btnScanQRStop.onclick = stopScan;

qrcode.callback = (res) => {
  if (res) {
    outputData.innerText = res;
    stopScan()
    qrResult.hidden = false;
    qrResultTitle.hidden = false;
  }
};

btnScanQR.onclick = () => {
  navigator.mediaDevices
    .getUserMedia({
      audio: false,
      image: {
        torch: true,
      },
      video: {
        facingMode: "environment",
        aspectRatio: 1,
        torch: true,
      }
    })
    .then(function(stream) {
      scanning = true;
      qrResult.hidden = true;
      qrResultTitle.hidden = true;
      btnScanQR.hidden = true;
      btnScanQRStop.hidden = false;
      canvasElement.hidden = false;

      // if (canvasElement.requestFullscreen) {
      //   canvasElement.requestFullscreen();
      // }
      // video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen

      video.srcObject = stream;
      scanTick = true;
      video.play();
      tick();
      scan();
    });
};


function tick() {
  canvasElement.height = video.videoHeight;
  canvasElement.width = video.videoWidth;
  canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);
  scanning && requestAnimationFrame(tick);
}

function scan() {
  try {
    qrcode.decode();
  } catch (e) {
    if (scanning) {
      setTimeout(scan, 300);
    }
  }
}

function onCapabilitiesReady(capabilities) {
  console.log(capabilities);
}
